<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class mbackend extends CI_Model{
	function __construct(){
		parent::__construct();
		//$this->auth = unserialize(base64_decode($this->session->userdata('sipbbg-k3pr1')));
	}
	
	function getdata($type="", $balikan="", $p1="", $p2=""){
		$where = " WHERE 1=1 ";
		if($this->input->post('key')){
				$where .=" AND ".$this->input->post('kat')." like '%".$this->db->escape_str($this->input->post('key'))."%'";
		}
		switch($type){
			case "member":
				//echo $p1;exit;
				if($p1=="get"){$where .=" AND A.member_id='".$this->input->post('id')."'";}
				$sql="SELECT A.*
					FROM tbl_member A  ".$where;
					//echo $sql;exit;
				if($p1=="get"){
					return $this->db->query($sql)->row_array();
				}
			break;
			case "transaksi":
				$sql="SELECT A.*,B.nama_lengkap as nm_costumer,C.alamat,F.nama_toko,
						G.nama_lengkap as nm_penjual
						FROM tbl_h_pemesanan A
						LEFT JOIN tbl_member B ON A.tbl_member_id=B.member_id
						LEFT JOIN tbl_alamat_member C ON A.tbl_alamat_member=C.id
						LEFT JOIN cl_jasapengiriman_layanan D ON A.cl_jasa_pengiriman_layanan_id=D.id
						LEFT JOIN cl_metode_pembayaran E ON A.cl_metode_pembayaran_id=E.id
						LEFT JOIN tbl_toko F ON A.tbl_toko_id=F.id
						LEFT JOIN tbl_member G ON F.tbl_member_id=G.member_id ";
			break;
			case "alamat":
				if($p1=="get"){$where .=" AND A.tbl_member_id='".$this->input->post('id')."'";}
				$sql="SELECT A.*,D.provinsi,C.kab_kota,B.kecamatan
					FROM tbl_alamat_member A  
					LEFT JOIN cl_kecamatan B ON A.cl_kecamatan_id=B.id
					LEFT JOIN cl_kab_kota C ON B.cl_kab_kota_kode=C.id
					LEFT JOIN cl_provinsi D ON C.cl_provinsi_kode=D.kode_prov ".$where;
				if($p1=="get"){
					return $this->db->query($sql)->result_array();
				}
			break;
			case "toko":
				if($p1=="get"){$where .=" AND A.tbl_member_id='".$this->input->post('id')."'";}
				$sql="SELECT A.*,A.deskripsi as deskripsi_toko,A.cl_kecamatan_id as cl_kecamatan_id_toko,
					A.alamat as alamat_toko,A.kode_pos as kode_pos_toko,D.kode_prov as kode_prov_toko,
					D.provinsi,C.kab_kota,B.kecamatan,B.cl_kab_kota_kode as cl_kab_kota_id_toko,A.id as id_toko
					FROM tbl_toko A  
					LEFT JOIN cl_kecamatan B ON A.cl_kecamatan_id=B.id
					LEFT JOIN cl_kab_kota C ON B.cl_kab_kota_kode=C.id
					LEFT JOIN cl_provinsi D ON C.cl_provinsi_kode=D.kode_prov ".$where;
				if($p1=="get"){
					return $this->db->query($sql)->row_array();
				}
			break;
			case "provinsi":
				$sql="SELECT A.*
					FROM cl_provinsi A  ";
			break;
			case "kabupaten":
				if($this->input->post('prov')!=""){$where .=" AND B.kode_prov='".$this->input->post('prov')."'";}
				if($this->input->post('key')!=""){$where .=" AND A.kab_kota like '%".$this->input->post('key')."%'";}
				if($p1=="get"){$where .=" AND B.kode_prov='".$p2."'";}
				$sql="SELECT A.*,B.provinsi,A.kab_kota as txt 
					FROM cl_kab_kota A 
					LEFT JOIN cl_provinsi B ON A.cl_provinsi_kode=B.kode_prov ".$where;
				
			break;
			case "kecamatan":
				if($this->input->post('prov')!=""){$where .=" AND C.kode_prov='".$this->input->post('prov')."'";}
				if($this->input->post('kab')!=""){$where .=" AND A.cl_kab_kota_kode='".$this->input->post('kab')."'";}
				if($this->input->post('key')!=""){$where .=" AND A.kecamatan like '%".$this->input->post('key')."%'";}
				if($p1!=""){
					if($p1=="get"){
						$where .=" AND A.cl_kab_kota_kode='".$p2."'";
					}else{
						$where .=" AND A.id=".$p1;
					}
				}
				$sql="SELECT A.*,C.kode_prov,C.provinsi,B.kab_kota,A.kecamatan as txt 
					FROM cl_kecamatan A
					LEFT JOIN cl_kab_kota B ON A.cl_kab_kota_kode=B.id 
					LEFT JOIN cl_provinsi C ON B.cl_provinsi_kode=C.kode_prov ".$where;
				if($p1!=""){
					if($p1!="get"){
						return $this->db->query($sql)->row_array();
					}
				}
			break;
			case "kelurahan":
				if($this->input->post('prov')!=""){$where .=" AND D.kode_prov='".$this->input->post('prov')."'";}
				if($this->input->post('kab')!=""){$where .=" AND B.cl_kab_kota_kode='".$this->input->post('kab')."'";}
				if($this->input->post('kec')!=""){$where .=" AND A.cl_kecamatan_id='".$this->input->post('kec')."'";}
				if($this->input->post('key')!=""){$where .=" AND A.kelurahan like '%".$this->input->post('key')."%'";}
				if($p1!=""){
					if($p1=="get"){$where .=" AND A.cl_kecamatan_id='".$p2."'";}
					else{$where .=" AND A.id=".$p1;}
				}
				$sql="SELECT A.*,D.kode_prov,D.provinsi,C.kab_kota,C.id as cl_kab_kota_id,B.id as cl_kecamatan_id,B.kecamatan,A.kelurahan as txt 
					FROM cl_kelurahan A 
					LEFT JOIN cl_kecamatan B ON A.cl_kecamatan_id=B.id
					LEFT JOIN cl_kab_kota C ON B.cl_kab_kota_kode=C.id 
					LEFT JOIN cl_provinsi D ON C.cl_provinsi_kode=D.kode_prov ".$where;
				if($p1!=""){
					if($p1!="get"){return $this->db->query($sql)->row_array();}
				}
				//echo $sql;exit;
			break;
			
			case "user":
				$sql = " 
					SELECT A.*
					FROM tbl_user_bo A WHERE A.nama_user='".$p1."' OR A.email='".$p1."'
				";
			break;
			case "cek_user":
				if($balikan=='get'){
					$sql = " SELECT A.*	FROM tbl_user_bo A 
							WHERE A.email='".$p1."'";
					$res=$this->db->query($sql)->row_array();
					return $res;
				}
				else{
					$sql = " SELECT A.*	FROM tbl_user_bo A 
							WHERE A.email='".$this->input->post('email')."'";
					$res=$this->db->query($sql)->row_array();
					if(isset($res['email'])){echo 2;}
					else echo 1;
					exit;
				}
			break;		
			case "tbl_user":
				$sql = " 
					SELECT A.*
					FROM tbl_user_bo A
				";
				if($p1=='edit'){
					$sql .=" WHERE A.id=".$p2;
				}
			break;		
			
		}
		
		if($balikan == 'row_array'){
			return $this->result_query($sql,'row_array');
		}elseif($balikan == 'result_array'){
			return $this->result_query($sql);
		}else{
			return $this->result_query($sql,'json');
		}
	}
	
	function get_combo($type="", $p1="", $p2=""){
		$where = " WHERE 1=1 ";
		switch($type){
			case "cl_provinsi":
				$sql="SELECT kode_prov as id,provinsi as txt FROM cl_provinsi ";
			break;
			case "cl_kab_kota":
				$sql="SELECT id,kab_kota as txt FROM cl_kab_kota ";
			break;
			default:
				$sql="SELECT * FROM ".$type;
			break;
		}
		
		return $this->db->query($sql)->result_array();
	}
	
	function result_query($sql,$type=""){
		switch($type){
			case "json":
				$page = (integer) (($this->input->post('page')) ? $this->input->post('page') : "1");
				$limit = (integer) (($this->input->post('rows')) ? $this->input->post('rows') : "10");
				$count = $this->db->query($sql)->num_rows();
				
				if( $count >0 ) { $total_pages = ceil($count/$limit); } else { $total_pages = 0; } 
				if ($page > $total_pages) $page=$total_pages; 
				$start = $limit*$page - $limit; // do not put $limit*($page - 1)
				if($start<0) $start=0;
				  
				$sql = $sql . " LIMIT $start,$limit";
			
				$data=$this->db->query($sql)->result_array();  
						
				if($data){
				   $responce = new stdClass();
				   $responce->rows= $data;
				   $responce->total =$count;
				   return json_encode($responce);
				}else{ 
				   $responce = new stdClass();
				   $responce->rows = 0;
				   $responce->total = 0;
				   return json_encode($responce);
				} 
			break;
			case "row_obj":return $this->db->query($sql)->row();break;
			case "row_array":return $this->db->query($sql)->row_array();break;
			default:return $this->db->query($sql)->result_array();break;
		}
	}
	
	// GOYZ CROTZZZ
	function simpan_data($table,$data,$get_id=""){ //$sts_crud --> STATUS NYEE INSERT, UPDATE, DELETE
		//print_r($data);exit;
		$this->db->trans_begin();
		$post = array();
		$id = $this->input->post('id');
		$field_id = "id";
		$sts_crud = $this->input->post('sts_crud');
		unset($data['sts_crud']);
		unset($data['id']);
		switch ($table){
			case "member":
				$table='tbl_member';
				$upload_path="__repo/member/";
				if(isset($data['cl_provinsi_kode_toko']))unset($data['cl_provinsi_kode_toko']);
				if(isset($data['cl_kab_kota_kode_toko']))unset($data['cl_kab_kota_kode_toko']);
				if(isset($data['cl_kecamatan_id_toko']))unset($data['cl_kecamatan_id_toko']);
				if(isset($data['nama_toko']))unset($data['nama_toko']);
				if(isset($data['deskripsi_toko']))unset($data['deskripsi_toko']);
				if(isset($data['alamat_toko']))unset($data['alamat_toko']);
				if(isset($data['kd_pos_toko']))unset($data['kd_pos_toko']);
				if(isset($data['id_toko']))unset($data['id_toko']);
				if($this->input->post('cl_kecamatan_id')){
					$cl_kecamatan_id=$data['cl_kecamatan_id'];
					$nama_penerima=$data['nama_penerima'];
					$no_hp_alm=$data['no_hp_alm'];
					$nama_alamat=$data['nama_alamat'];
					$alamat=$data['alamat'];
					$kode_pos=$data['kode_pos'];
					unset($data['cl_kecamatan_id']);unset($data['nama_penerima']);
					unset($data['no_hp_alm']);unset($data['nama_alamat']);unset($data['alamat']);unset($data['kode_pos']);
				}
				if ($sts_crud=='add'){
					$id=strtoupper(uniqid());
					$data["member_id"]=$id;
					$data["pwd"]=$this->encrypt->encode('12345');
				}else{$id = $this->input->post('id');}
				if(isset($_FILES['foto']) && $_FILES['foto']['name']!=''){
					$upload_path = $upload_path.$id."/";
					$object='foto';
					$data['foto']=$this->lib->uploadnong($upload_path, $object, date('YmdHis').'_'.$id);
				}
				
				if($this->input->post('nama_toko')){//UNTUKK TOKO
					$data_toko=array();
					$id_toko=$this->input->post('id_toko');
					$data_toko['tbl_member_id']=$id;
					$data_toko['cl_kecamatan_id']=$this->input->post('cl_kecamatan_id_toko');
					$data_toko['nama_toko']=$this->input->post('nama_toko');
					$data_toko['deskripsi']=$this->input->post('deskripsi_toko');
					$data_toko['alamat']=$this->input->post('alamat_toko');
					$data_toko['kode_pos']=$this->input->post('kd_pos_toko');
					
					
					if(isset($_FILES['logo']) && $_FILES['logo']['name']!=''){
						$upload_path = $upload_path.$id."/logo/";
						$object='logo';
						$data_toko['logo']=$this->lib->uploadnong($upload_path, $object, date('YmdHis').'_'.$id);
					}
				}
				
				
				//print_r($data_toko);exit;
			break;
			default:$table = "tbl_".$table;break;
		}
		
		if($sts_crud == 'add'){
			$data['create_date'] = date('Y-m-d H:i:s');
			$data['create_by'] = $this->auth['nama_user'];
			$this->db->insert($table, $data);
			if($table=='tbl_member'){
				if($this->input->post('cl_kecamatan_id')){
					if(count($cl_kecamatan_id)>0){
						$data_alm=array();
						foreach($cl_kecamatan_id as $x=>$v){
							$data_alm[]=array('tbl_member_id'=>$data['member_id'],
											  'cl_kecamatan_id'=>$v,
											  'nama_penerima'=>$nama_penerima[$x],
											  'no_hp_alm'=>$no_hp_alm[$x],
											  'nama_alamat'=>$nama_alamat[$x],
											  'alamat'=>$alamat[$x],
											  'kode_pos'=>$kode_pos[$x],
											  'create_date'=>date('Y-m-d H:i:s'),
											  'create_by'=>$this->auth['nama_user']
							);
						}
						$this->db->insert_batch('tbl_alamat_member', $data_alm);
					}
				}
				if($this->input->post('nama_toko')){$this->db->insert('tbl_toko', $data_toko);}
			}
		}elseif($sts_crud == 'edit'){
			if($table=="tbl_member"){
				$this->db->update($table, $data, array('member_id'=>$id) );
				$this->db->delete('tbl_alamat_member', array('tbl_member_id'=>$id) );
				if($this->input->post('cl_kecamatan_id')){
					if(count($cl_kecamatan_id)>0){
						$data_alm=array();
						foreach($cl_kecamatan_id as $x=>$v){
							$data_alm[]=array('tbl_member_id'=>$id,
											  'cl_kecamatan_id'=>$v,
											  'nama_penerima'=>$nama_penerima[$x],
											  'no_hp_alm'=>$no_hp_alm[$x],
											  'nama_alamat'=>$nama_alamat[$x],
											  'alamat'=>$alamat[$x],
											  'kode_pos'=>$kode_pos[$x],
											  'create_date'=>date('Y-m-d H:i:s'),
											  'create_by'=>$this->auth['nama_user']
							);
						}
						$this->db->insert_batch('tbl_alamat_member', $data_alm);
					}
				}
				if($this->input->post('nama_toko')){
					//CEK TOKO
					$rs=$this->db->get_where('tbl_toko',array('tbl_member_id'=>$id))->row_array();
					if(isset($rs['tbl_member_id']))$this->db->update('tbl_toko', $data_toko,array('id'=>$id_toko));
					else $this->db->insert('tbl_toko', $data_toko);
					
				}
			}
			else $this->db->update($table, $data, array($field_id=>$id) );
		}elseif($sts_crud == 'delete'){
			if($table=="tbl_member"){
				$this->db->delete('tbl_alamat_member',array('tbl_member_id'=>$id) );
				$this->db->delete('tbl_toko',array('tbl_member_id'=>$id) );
				$this->db->delete($table,array('member_id'=>$id) );
			}
			else $this->db->delete($table, array($field_id=>$id) );
		}
		
		
		
		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			return 0;
		}else{
			if($table=='tbl_responden'){
				$this->db->trans_commit();
				return json_encode(array('id_resp'=>$id_resp,'sts'=>1));
			}
			else{
				return $this->db->trans_commit();	
			}
		}
	}
	
	function simpan_reg($p1="",$p2=""){
		$this->db->trans_begin();
		$post = array();
        foreach($_POST as $k=>$v){if($this->input->post($k)!=""){$post[$k] = $this->db->escape_str($this->input->post($k));}}
		//print_r($post);exit;
		
		if($p1=="act"){
			$post['status']=1;
			$post['act_date']=date('Y-m-d H:i:s');
			$this->db->where('email',$p2);
			$this->db->update('tbl_user_bo',$post);
		}else{
			unset($post['password2']);
			//unset($post['password']);
			$post['password']=$this->encrypt->encode($post['password']);
			$post['reg_date']=date('Y-m-d H:i:s');
			$post['status']=0;
			$post['cl_user_group_id']=2;
			$this->db->insert('tbl_user_bo', $post);
		}
		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			return 0;
		}else{
			return $this->db->trans_commit();
			
		}
	}
		
}