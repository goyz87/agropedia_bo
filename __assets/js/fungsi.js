var acak_form;
var id_responden='';
var peta;
var pertama = 0;
var jenis = "restoran";
var judulx = new Array();
var desx = new Array();
var i;
var url;
var gambar_tanda;
var markersArray=[];
function loadUrl(urls){
	//$("#konten").empty();
    $("#konten").empty().addClass("loading");
   // $("#konten").html("").addClass("loading");
	$.get(urls,function (html){
	    $("#konten").html(html).removeClass("loading");
    });
}

function getClientHeight(){
	var theHeight;
	if (window.innerHeight)
		theHeight=window.innerHeight;
	else if (document.documentElement && document.documentElement.clientHeight) 
		theHeight=document.documentElement.clientHeight;
	else if (document.body) 
		theHeight=document.body.clientHeight;
	
	return theHeight;
}

var divcontainer;
function windowFormPanel(html,judul,width,height){
	divcontainer = $('#jendela');
	$(divcontainer).unbind();
	$('#isiJendela').html(html);
    $(divcontainer).window({
		title:judul,
		width:width,
		height:height,
		autoOpen:false,
		top: Math.round(frmHeight/2)-(height/2),
		left: Math.round(frmWidth/2)-(width/2),
		modal:true,
		maximizable:false,
		minimizable: false,
		collapsible: false,
		closable: true,
		resizable: false,
	    onBeforeClose:function(){	   
			$(divcontainer).window("close",true);
			//$(divcontainer).window("destroy",true);
			//$(divcontainer).window('refresh');
			return true;
	    }		
    });
    $(divcontainer).window('open');       
}
function windowFormClosePanel(){
    $(divcontainer).window('close');
	//$(divcontainer).window('refresh');
}

var container;
function windowForm(html,judul,width,height){
    container = "win"+Math.floor(Math.random()*9999);
    $("<div id="+container+"></div>").appendTo("body");
    container = "#"+container;
    $(container).html(html);
    $(container).css('padding','5px');
    $(container).window({
       title:judul,
       width:width,
       height:height,
       autoOpen:false,
       maximizable:false,
       minimizable: false,
	   collapsible: false,
       resizable: false,
       closable:true,
       modal:true,
	   onBeforeClose:function(){	   
			$(container).window("close",true);
			$(container).window("destroy",true);
			return true;
	   }
    });
    $(container).window('open');        
}
function closeWindow(){
    $(container).window('close');
    $(container).html("");
}


function getClientWidth(){
	var theWidth;
	if (window.innerWidth) 
		theWidth=window.innerWidth;
	else if (document.documentElement && document.documentElement.clientWidth) 
		theWidth=document.documentElement.clientWidth;
	else if (document.body) 
		theWidth=document.body.clientWidth;

	return theWidth;
}


function genGrid(modnya, divnya, lebarnya, tingginya){
	if(lebarnya == undefined){
		lebarnya = (getClientWidth()-250);
	}
	if(tingginya == undefined){
		tingginya = (getClientHeight()-300);
	}

	var kolom ={};
	var frozen ={};
	var judulnya;
	var param={};
	var urlnya;
	var urlglobal="";
	var url_detil="";
	var post_detil={};
	var fitnya;
	var klik=false;
	var doble_klik=false;
	var pagesizeboy = 15;
	var singleSelek = true;
	var nowrap_nya = true;
	var footer=false;
	
	switch(modnya){
		case "transaksi":
			judulnya = "";
			urlnya = "transaksi";
			fitnya = true;
			urlglobal = host+'backend/getdata/'+urlnya;
			kolom[modnya] = [	
				{field:'status',title:'Status',width:200, halign:'center',align:'center',
					formatter: function(value,row,index){
						if(value=='PP')return "Proses Pembayaran";
						else if(value=='PV')return "Proses Verifikasi Pembayaran";
						else if(value=='DP')return "Diteruskan Ke Penjual";
						else if(value=='DO')return "DiProses Oleh Penjual";
						else if(value=='PK')return "Proses Pengiriman";
						else return "-";
					}
				},
				{field:'no_order',title:'No. Order',width:100, halign:'center',align:'left'},
				{field:'tgl_order',title:'Tgl',width:120, halign:'center',align:'left'},
				{field:'nm_costumer',title:'Customer.',width:180, halign:'center',align:'left'},
				{field:'nm_penjual',title:'Penjual',width:180, halign:'center',align:'left',
					formatter: function(value,row,index){
						return row.nama_toko + '( '+value+' )'
					}
				},
				{field:'sub_total',title:'Sub. Total',width:100, halign:'right',align:'right',
					formatter: function(value,row,index){
						return NumberFormat(value);
					}
				},
				{field:'total_ongkir',title:'Ongkir',width:100, halign:'right',align:'right',
					formatter: function(value,row,index){
						return NumberFormat(value);
					}
				},
				{field:'grand_total',title:'GrandTotal',width:100, halign:'right',align:'right',
					formatter: function(value,row,index){
						return NumberFormat(value);
					}
				}
			]
		break;
		case "member":
			judulnya = "";
			urlnya = "member";
			fitnya = true;
			urlglobal = host+'backend/getdata/'+urlnya;
			kolom[modnya] = [	
				{field:'flag',title:'Status',width:100, halign:'center',align:'center',
					formatter: function(value,row,index){
						if(value=='Y')return "Aktif";
						else return "Tidak Aktif";
					}
				},
				{field:'member_id',title:'Member ID',width:150, halign:'center',align:'left'},
				{field:'email',title:'Email',width:150, halign:'center',align:'left'},
				{field:'nama_lengkap',title:'Nama Lengkap.',width:180, halign:'center',align:'left'},
				{field:'tgl_lahir',title:'Tgl Lahir.',width:100, halign:'center',align:'center'},
				{field:'jenis_kelamin',title:'Jenis Kelamin',width:120, halign:'center',align:'center',
					formatter: function(value,row,index){
						if(value=='P')return "Pria";
						else return "Wanita";
					}
				},
				{field:'no_hp',title:'No HP',width:100, halign:'center',align:'center'}
			]
		break;
		case "responden":
			judulnya = "";
			urlnya = "responden";
			fitnya = false;
			urlglobal = host+'backend/getdata/'+urlnya;
			
			kolom[modnya] = [	
				{field:'nomor_kuesioner',title:'No Kues.',width:150, halign:'center',align:'left'},
				{field:'periode_awal',title:'Periode',width:250, halign:'center',align:'center',
					formatter: function(value,row,index){
						return value +" s/d "+ row.periode_akhir;
					}
				},
				{field:'kab_kota',title:'Kab. Kec.',width:300, halign:'center',align:'left',
					formatter: function(value,row,index){
						return value +" KEC. "+ row.kecamatan;
					}
				},
				{field:'nama_responden',title:'Responden',width:300, halign:'center',align:'left'},
				{field:'alamat_responden',title:'Alamat',width:300, halign:'center',align:'left'},
				
			]
		break;
	}
	
	grid_nya=$("#"+divnya).datagrid({
		title:judulnya,
        height:tingginya,
        width:lebarnya,
		rownumbers:true,
		iconCls:'database',
        fit:fitnya,
        striped:true,
        pagination:true,
        remoteSort: false,
		showFooter:footer,
		singleSelect:singleSelek,
        url: urlglobal,		
		nowrap: nowrap_nya,
		pageSize:pagesizeboy,
		pageList:[15,25,50,75,100,200],
		queryParams:param,
		frozenColumns:[
            frozen[modnya]
        ],
		columns:[
            kolom[modnya]
        ],
		onLoadSuccess:function(d){
			//gridVRList.datagrid('selectRow', 0);
			$('.yes').linkbutton({  
					iconCls: 'icon-cancel'  
			});
			$('.no').linkbutton({  
					iconCls: 'icon-ok'  
			});
			
		},
		onClickRow:function(rowIndex,row){
			switch(modnya){
				case "responden":
					$('#id_'+acak_form).val(row.id);
					$('#sts_crud_'+acak_form).val('edit');
					$('#nomor_kuesioner_'+acak_form).val(row.nomor_kuesioner);
					$('#periode_'+acak_form).val(row.periode);
					$('#cl_kabupaten_id_'+acak_form).val(row.cl_kabupaten_id).change();
					fillCombo(host+"backend/get_combo", "cl_kecamatan_id_"+acak_form, "kecamatan",row.cl_kabupaten_id,row.cl_kecamatan_id);
					$('#tbl_relawan_id_'+acak_form).val(row.tbl_relawan_id).change();
					$('#nama_responden_'+acak_form).val(row.nama_responden);
					$('#alamat_responden_'+acak_form).val(row.alamat_responden);
					if(row.jenis_kelamin=='L')$('input:radio[name=jenis_kelamin]:nth(0)').attr('checked',true);
					else $('input:radio[name=jenis_kelamin]:nth(1)').attr('checked',true);
				break;
			}
			closeWindow();
        },
		onDblClickRow:function(rowIndex,row){
			
		},
		toolbar: '#tb_'+modnya,
		rowStyler: function(index,row){
			if(modnya == 'reservasi'){
				if (row.flag == 1){
					return 'background-color:#C5FFC2;'; // return inline style
				}else if(row.flag == 0){
					return 'background-color:#FFD1BB;'; // return inline style
				}
			}
			
		},
		onLoadSuccess: function(data){
			if(data.total == 0){
				var $panel = $(this).datagrid('getPanel');
				var $info = '<div class="info-empty" style="margin-top:20%;">Data Tidak Tersedia</div>';
				$($panel).find(".datagrid-view").append($info);
				//$('#edit').linkbutton({disabled:true});
				//$('#del').linkbutton({disabled:true});
			}else{
				$($panel).find(".datagrid-view").append('');
			}
		},
	});
}


function genform(type, modulnya, submodulnya, stswindow, tabel){
	var urlpost = host+'backend/getdisplay/get-form/'+submodulnya;
	var urldelete = host+'backend/cruddata/'+submodulnya;
	var id_tambahan = "";
	
	switch(submodulnya){
		case "member":table="tbl_member";break;
		case "kabupaten":table="cl_kab_kota";break;
		case "kecamatan":table="cl_kecamatan";break;
		case "kelurahan":table="cl_kelurahan";break;
		case "posko":table="tbl_posko";break;
		case "relawan":table="tbl_relawan";break;
		
	}
	
	switch(type){
		case "add":
			if(stswindow == undefined){
				$('#grid_nya_'+submodulnya).hide();
				$('#detil_nya_'+submodulnya).empty().show().addClass("loading");
			}
			$.post(urlpost, {'editstatus':'add', 'id_tambahan':id_tambahan }, function(resp){
				if(stswindow == 'windowform'){
					windowForm(resp, judulwindow, lebar, tinggi);
				}else if(stswindow == 'windowpanel'){
					windowFormPanel(resp, judulwindow, lebar, tinggi);
				}else{
					$('#detil_nya_'+submodulnya).show();
					$('#detil_nya_'+submodulnya).html(resp).removeClass("loading");
				}
			});
		break;
		case "edit":
		case "delete":
		
			var row = $("#grid_"+submodulnya).datagrid('getSelected');
			if(row){
				if(type=='edit'){
					if(stswindow == undefined){
						$('#grid_nya_'+submodulnya).hide();
						$('#detil_nya_'+submodulnya).show().addClass("loading");	
					}
					$.post(urlpost, { 'editstatus':'edit', id:(submodulnya=='member' ? row.member_id : row.id), 'ts':table, 'submodul':submodulnya, 'bulan':row.bulan, 'tahun':row.tahun }, function(resp){
						if(stswindow == 'windowform'){
							windowForm(resp, judulwindow, lebar, tinggi);
						}else if(stswindow == 'windowpanel'){
							windowFormPanel(resp, judulwindow, lebar, tinggi);
						}else{
							$('#detil_nya_'+submodulnya).show();
							$('#detil_nya_'+submodulnya).html(resp).removeClass("loading");
						}
					});
				}else if(type=='delete'){
					//if(confirm("Anda Yakin Menghapus Data Ini ?")){
					$.messager.confirm('SIM PILKADA','Anda Yakin Menghapus Data Ini ?',function(re){
						if(re){
							loadingna();
							$.post(urldelete, {id:(submodulnya=='member' ? row.member_id : row.id), 'sts_crud':'delete'}, function(r){
								if(r==1){
									winLoadingClose();
									$.messager.alert('SIM PILKADA',"Data Terhapus",'info');
									$('#grid_'+submodulnya).datagrid('reload');								
								}else{
									winLoadingClose();
									console.log(r)
									$.messager.alert('SIM PILKADA',"Gagal Menghapus Data",'error');
								}
							});	
						}
					});	
					//}
				}
				
			}
			else{
				$.messager.alert('SIM PILKADA',"Select Row In Grid",'error');
			}
		break;
		
	}
}

function kumpulAction(type, p1, p2, p3, p4, p5){
	switch(type){
		case "reservation":
			grid = $('#grid_reservasi').datagrid('getSelected');
			$.post(host+'backend/simpan_data/tbl_reservasi_confirm', { 'id':grid.id, 'confirm':p1 }, function(rsp){
				if(rsp == 1){
					$.messager.alert('Roger Salon',"Confirm OK",'info');
				}else{
					$.messager.alert('Roger Salon',"Failed Confirm",'error');
				}
				$('#grid_reservasi').datagrid('reload');	
			} );
		break;
		case "banner":
			grid = $('#grid_banner').datagrid('getSelected');
			$.post(host+'backend/simpan_data/tbl_banner_confirm', { 'id':grid.id, 'confirm':p1 }, function(rsp){
				if(rsp == 1){
					$.messager.alert('Roger Salon',"OK",'info');
				}else{
					$.messager.alert('Roger Salon',"Gagal",'error');
				}
				$('#grid_banner').datagrid('reload');	
			} );
		break;
		case "hapus_produk":
			$('#detail_fotonya').empty().addClass("loading");
			$.post(host+'backend/hapusfoto_detail/produk', { 'id':p1, 'nama_file':p2, 'id_header':p3  }, function(rsp){
				$('#detail_fotonya').html(rsp).removeClass("loading");
			} );
		break;
		case "hapus_service":
			$('#detail_fotonya').empty().addClass("loading");
			$.post(host+'backend/hapusfoto_detail/service', { 'id':p1, 'nama_file':p2, 'id_header':p3  }, function(rsp){
				$('#detail_fotonya').html(rsp).removeClass("loading");
			} );
		break;
	}
}	

function submit_form(frm,func){
	var url = jQuery('#'+frm).attr("url");
	if ($('#'+frm).form('validate')){
		jQuery('#'+frm).form('submit',{
				url:url,
				onSubmit: function(){
					  return $(this).form('validate');
				},
				success:function(data){
					//$.unblockUI();
					if (func == undefined ){
						 if (data == "1"){
							pesan('Data Sudah Disimpan ','Sukses');
						}else{
							 pesan(data,'Result');
						}
					}else{
						func(data);
					}
				},
				error:function(data){
					//$.unblockUI();
					 if (func == undefined ){
						 pesan(data,'Error');
					}else{
						func(data);
					}
				}
		});
	}else{
		$.messager.alert('AGROPEDIA BO','Harap Isi Data Yang Kosong', 'error');
	}
}

function fillCombo(url, SelID, value, value2, value3, value4){
	//if(Ext.get(SelID).innerHTML == "") return false;
	if (value == undefined) value = "";
	if (value2 == undefined) value2 = "";
	if (value3 == undefined) value3 = "";
	if (value4 == undefined) value4 = "";
	
	$('#'+SelID).empty();
	$.post(url, {"v": value, "v2": value2, "v3": value3, "v4": value4},function(data){
		$('#'+SelID).append(data);
	});

}
function formatDate(date) {
	var bulan=date.getMonth() +1;
	var tgl=date.getDate();
	if(bulan < 10){
		bulan='0'+bulan;
	}
	
	if(tgl < 10){
		tgl='0'+tgl;
	}
	return date.getFullYear() + "-" + bulan + "-" + tgl;
}

function myparser(s){
    if (!s) return new Date();
    var ss = (s.split('-'));
    var y = parseInt(ss[0],10);
    var m = parseInt(ss[1],10);
    var d = parseInt(ss[2],10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
        return new Date(y,m-1,d);
    } else {
        return new Date();
    }
}
function clear_form(id){
	$('#'+id).find("input[type=text], textarea,select").val("");
	//$('.angka').numberbox('setValue',0);
}

var divcontainerz;
function windowLoading(html,judul,width,height){
    divcontainerz = "win"+Math.floor(Math.random()*9999);
    $("<div id="+divcontainerz+"></div>").appendTo("body");
    divcontainerz = "#"+divcontainerz;
    $(divcontainerz).html(html);
    $(divcontainerz).css('padding','5px');
    $(divcontainerz).window({
       title:judul,
       width:width,
       height:height,
       autoOpen:false,
       modal:true,
       maximizable:false,
       resizable:false,
       minimizable:false,
       closable:false,
       collapsible:false,  
    });
    $(divcontainerz).window('open');        
}
function winLoadingClose(){
    $(divcontainerz).window('close');
    //$(divcontainer).html('');
}
function loadingna(){
	windowLoading("<img src='"+host+"__assets/img/loading.gif' style='position: fixed;top: 50%;left: 50%;margin-top: -10px;margin-left: -25px;'/>","Please Wait",200,100);
}

function NumberFormat(value) {
	
    var jml= new String(value);
    if(jml=="null" || jml=="NaN") jml ="0";
    jml1 = jml.split("."); 
    jml2 = jml1[0];
    amount = jml2.split("").reverse();

    var output = "";
    for ( var i = 0; i <= amount.length-1; i++ ){
        output = amount[i] + output;
        if ((i+1) % 3 == 0 && (amount.length-1) !== i)output = '.' + output;
    }
    //if(jml1[1]===undefined) jml1[1] ="00";
   // if(isNaN(output))  output = "0";
    return output; // + "." + jml1[1];
}

function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	}
function konversi_pwd_text(id){
	if($('input#'+id)[0].type=="password")$('input#'+id)[0].type = 'text';
	else $('input#'+id)[0].type = 'password';
}
function hapus_file(mod,id,id_list){
	loadingna();
	$.post(host+'HapusFile',{mod:mod,id:id},function(r){
		if(r==1){
			winLoadingClose();
			$('#'+id_list).remove();
		}else{
			console.log(r);
			winLoadingClose();
			$.messager.alert('Aldeaz',"Gagal Menghapus File",'error');
		}
	});
}
function initMap() {
	  var myLatlng = new google.maps.LatLng(-6.381631, 120.382690);
	  var myOptions = {
		  zoom: 7,
		  center: myLatlng,
		  gestureHandling: 'greedy'
	  };
	  peta = new google.maps.Map(document.getElementById("map"), myOptions);
	
}
function ceklks(lang){
	$.post(host+'lihat-lokasi', { 'valnya':$('#lokasi').val(), 'lang':lang }, function(resp){
		var respon=JSON.parse(resp);
		console.log(respon.lat);
		console.log(respon.longi);
		$('#lokasinya').html(respon.cetak);
		var myLatlng = new google.maps.LatLng(respon.longi,respon.lat);
        var myOptions = {
            zoom: 13,
            center: myLatlng
        };
              
//              menampilkan output pada element
        var map = new google.maps.Map(document.getElementById("map"), myOptions);
              
//              menambahkan marker
        var marker = new google.maps.Marker({
             position: myLatlng,
             map: map,
             title:"Monas"
        });

	} );
}

function chart_na(id_selector,type,title,subtitle,title_y,data_x,data_y,satuan){
	switch(type){
	case "column":
	$('#'+id_selector).highcharts({
			chart: {
				type: type
			},
			title: {
				text: title
			},
			subtitle: {
				text: subtitle
			},
			xAxis:{
				type: 'category'
			},
			yAxis: {
				title: {
					text: title_y
				}

			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y:.1f}'
					}
				}
			},

			tooltip: {
				headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
				pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
			},

			series: data_x
			
			
			
        });
		break;
		case "line" :
			$('#'+id_selector).highcharts({
				title: {
				text: title,
				x: -20 //center
				},
				subtitle: {
					text: subtitle,
					x: -20
				},
				xAxis: {
					categories: data_y
				},
				yAxis: {
					title: {
						text: 'Jumlah Dukungan'
					},
					plotLines: [{
						value: 0,
						width: 1,
						color: '#808080'
					}]
				},
				tooltip: {
					valueSuffix: 'Total'
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'middle',
					borderWidth: 0
				},
				series: data_x
			});
		break;
		case "pie":
			 $('#'+id_selector).highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: title
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: false
						},
						showInLegend: true
					}
				},
				series: data_y
			});
		break;
	}
}
function get_lokasi(wil){
	var ex={};
	var zoom;
	 //alert(wil);
	switch(wil){
		case 'BOGOR':
			ex['long']=106.79981460668944;
			ex['lat']=-6.599092201665239;
			zoom=14;
		break;
		
		case 'All':
			ex['long']=117.19912325500002;
			ex['lat']=-0.6333576479565592;
			zoom=5;
		break;
		case 'SULSEL':
			ex['long']=119.975703;
			ex['lat']=-3.668300;
			zoom=7;
		break;
		
		case 'DKI':
			ex['long']=106.82822456457518;
			ex['lat']= -6.177210450737989;
			zoom=14;
		break;
		
	}
	
	peta.setZoom(zoom);
	peta.setCenter(new google.maps.LatLng(ex['lat'],ex['long']));
	
}

function set_icon(jenisnya){
    switch(jenisnya){
        case "jual":
            gambar_tanda = host+'asset/img/lap_laba.png';
            break;
        case "beli":
            gambar_tanda = host+'asset/img/kredit_jual.png';
            break;
        case "stock":
            gambar_tanda = host+'asset/img/database.png';
            break;
		case "point":
            gambar_tanda = host+'__assets/img/zone.png';
            break;
    }
}

function setjenis(obj){
	//var restoranChk,airportChk,mesjidChk;
		objId=obj.id;
    	jenis = objId;
		//alert(jenis)
		class_na(objId);
		//alert(objId);
		switch(objId){
			case 'jual':
				$('#beli').addClass('legend-hide');
				$('#stock').addClass('legend-hide');
				clearOverlays() 
				ambildatabase('jual');
			break;	
				
			case 'beli':
				$('#jual').addClass('legend-hide');
				$('#stock').addClass('legend-hide');
				clearOverlays() 
				ambildatabase('beli');
			break;	
				
			case 'stock':
				$('#beli').addClass('legend-hide');
				$('#jual').addClass('legend-hide');	
				clearOverlays() 
				ambildatabase('stock');
			break;	
		}	
}
function setinfo(petak, nomor){
    google.maps.event.addListener(petak, 'click', function() {
		//alert (nomor);
		$.post(host+'backend/getdisplay/get_info/',{kec:nomor},function(resp){
			windowForm(resp,'INFORMASI HASIL SURVEY',(getClientWidth()-200),500);
		});
        //$("#jendelainfo").fadeIn();
        //$("#teksjudul").html(judulx[nomor]);
        //$("#teksdes").html(desx[nomor]);
    });
}
function ambildatabase(){
	url = host+"backend/get_point/";
    $.post(host+"backend/get_point/",function(r){
			var a=JSON.parse(r);
			$.each(a.wilayah, function(idx,grp){
				//console.log(grp.x);
				judulx[i] = grp.kec;
                set_icon('point');
                var point = new google.maps.LatLng(
                    parseFloat(grp.x),
                    parseFloat(grp.y)
				);
                tanda = new google.maps.Marker({
                    position: point,
					animation: google.maps.Animation.DROP,
                    map: peta,
					label:{
						text: grp.kec,
						fontSize: "8px"
						//color: 'white'
					},
                    icon: host+'__assets/img/marker.png'
                });
				markersArray.push(tanda);
                setinfo(tanda,grp.id);
			});
    });
}
function kasihtanda(lokasi){
    set_icon(jenis);
    tanda = new google.maps.Marker({
            position: lokasi,
            map: peta,
            icon: gambar_tanda
    });
}
function clearOverlays() {
  for (var i = 0; i < markersArray.length; i++ ) {
	//  console.log(markersArray[i]);
    markersArray[i].setMap(null);
  }
}
function cariData(mod,acak){
	var _post={};
	/*_post['prov']=$('#prov_'+acak).val();
	_post['kab']=$('#kab_'+acak).val();
	_post['kec']=$('#kec_'+acak).val();
	_post['kel']=$('#kel_'+acak).val();
	*/
	_post['kat']=$('#kat_'+acak).val();
	_post['key']=$('#key_'+acak).val();
	grid_nya.datagrid('reload',_post);
}
function get_alat_peraga(acak){
	var _post={};
	_post["kel"]=$('#cl_kelurahan_id_alat_peraga_'+acak).val();
	_post["id_responden"]=id_responden;
	$('#list_alat_peraga').html('').addClass('loading');
	$.post(host+'backend/getdisplay/alat_peraga',_post,function(r){
		$('#list_alat_peraga').removeClass('loading').html(r);
	});
}
var newWindow;
function openWindowWithPost(url,params)
{
    var x = Math.floor((Math.random() * 10) + 1);
	
	if (!newWindow || typeof(newWindow)=="undefined"){
		newWindow = window.open(url, 'winpost'); 
	}else{
		newWindow.close();
		newWindow = window.open(url, 'winpost'); 
		//return false;
	}
	
	var formid= "formid"+x;
    var html = "";
    html += "<html><head></head><body><form  id='"+formid+"' method='post' action='" + url + "'>";

    $.each(params, function(key, value) {
        if (value instanceof Array || value instanceof Object) {
            $.each(value, function(key1, value1) { 
                html += "<input type='hidden' name='" + key + "["+key1+"]' value='" + value1 + "'/>";
            });
        }else{
            html += "<input type='hidden' name='" + key + "' value='" + value + "'/>";
        }
    });
   
    html += "</form><script type='text/javascript'>document.getElementById(\""+formid+"\").submit()</script></body></html>";
    newWindow.document.write(html);
    return newWindow;
}
function tambah_row(mod,param){
	var tr_table;
	switch(mod){
		case "member":
			$('.tr_member_0').remove();
			idx_row++;
			tr_table +='<tr class="tr_member" id="tr_member_'+idx_row+'" idx='+idx_row+'>';
			tr_table +='<td width="" style="text-align:left;"><input type="hidden" name="nama_alamat[]" id="nama_alamat_'+idx_row+'" style="width:100%;height:27px;" value="'+$('#nm_alamat_'+param).val()+'">'+$('#nm_alamat_'+param).val()+'</td>';
			tr_table +='<td width="" style="text-align:left;"><input type="hidden" name="cl_kecamatan_id[]" id="cl_kecamatan_id_'+idx_row+'" style="width:100%;height:27px;" value="'+$('#cl_kecamatan_id_'+param).val()+'">KEC. '+$("#cl_kecamatan_id_"+param+" option:selected").text()+'  '+$("#cl_kab_kota_kode_"+param+" option:selected").text()+' PROV. '+$("#cl_provinsi_kode_"+param+" option:selected").text()+'</td>';
			tr_table +='<td width="" style="text-align:left;"><input type="hidden" name="alamat[]" id="alamat_'+idx_row+'" style="width:100%;height:27px;" value="'+$('#almt_'+param).val()+'">'+$('#almt_'+param).val()+'</td>';
			tr_table +='<td width="" style="text-align:center;"><input type="hidden" name="kode_pos[]" id="kode_pos_'+idx_row+'" style="width:100%;height:27px;" value="'+$('#kd_pos_'+param).val()+'">'+$('#kd_pos_'+param).val()+'</td>';
			tr_table +='<td width="" style="text-align:left;"><input type="hidden" name="no_hp_alm[]" id="no_hp_'+idx_row+'" style="width:100%;height:27px;" value="'+$('#n_hp_'+param).val()+'">'+$('#n_hp_'+param).val()+'</td>';
			tr_table +='<td width="" style="text-align:left;"><input type="hidden" name="nama_penerima[]" id="nama_penerima_'+idx_row+'" style="width:100%;height:27px;" value="'+$('#nm_penerima_'+param).val()+'">'+$('#nm_penerima_'+param).val()+'</td>';
			tr_table +='<td><a href="javascript:void(0);" class="btn btn-danger btn-circle" onclick="$(this).parents(\'tr\').first().remove();"><i class="fa fa-times"></i></a></td>';
			tr_table +='</tr>';
		break;
		case "bd_inv":
			tr_table +='<tr class="tr_inv" id="tr_inv_'+idx_row+'" idx='+idx_row+'>';
			tr_table +='<td><select id="cl_layanan_id_'+idx_row+'" row="'+idx_row+'" name="cl_layanan_id[]" required="" class="form-control validasi cek_hpp" style="padding:0;height:27px;font-size:11px;">';
			tr_table +=opt_layanan;
			tr_table +='</select></td>';
			tr_table +='	<td><input type="text" name="nama[]" style="width:100%;height:27px;" class="validasi"></td>';
			tr_table +='	<td style="text-align:left;"><span id="keterangan_'+idx_row+'"></span> </td>';
			tr_table +='	<td style="text-align:right;"><input type="hidden" name="hpp[]" id="val_hpp_'+idx_row+'"><span id="hpp_'+idx_row+'"></span></td>';
			tr_table +='	<td style="text-align:right;"><input type="text" name="hpp_penawaran[]" class="angka tot" style="height:28px;"> </td>';
			tr_table +='	<td><a href="javascript:void(0);" class="btn btn-danger btn-circle" onclick="$(this).parents(\'tr\').first().remove();"><i class="fa fa-times"></i></a></td>';
			tr_table +='</tr>';
		break;
		
	}
	
	$('.bd_'+mod).append(tr_table);
	$(".validasi").validatebox({ required:true }); 
	/*$(".angka").numberbox({ required:true,min:0,precision:0,groupSeparator:'.',decimalSeparator:',',
		onChange:function(){
			get_total('qty','harga_beli','total',idx_row);
		}
	}); */
}